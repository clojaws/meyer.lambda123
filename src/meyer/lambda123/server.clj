(ns meyer.lambda123.server
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [meyer.lambda123.service :as service])
  (:gen-class
   :main true
   :name meyer.lambda123.Server))

(def service-dev
  {:env :dev
   ::http/join? false
   :io.pedestal.http/type :jetty
   :io.pedestal.http/routes service/routes})

(def service-prod
  {:io.pedestal.http/type :jetty
   :io.pedestal.http/routes service/routes})

(defonce runnable-service (http/create-server (service/service service-prod)))

(defonce runnable-dev (atom nil))

(defn run-dev
  []
  (println "foooooooobarrrrrr....")
  (->> (service/service service-dev)
       http/default-interceptors
       http/dev-interceptors
       http/create-server
       http/start
       (reset! runnable-dev)))

(defn stop-dev
  []
  (println "boooobarrrrr.....")
  (http/stop @runnable-dev))

(defn -main
  [& args]
  (println (service/service {}))
  (http/start runnable-service))
