(ns meyer.lambda123.service
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            
            [io.pedestal.interceptor :as interceptor]
            [cheshire.core :as json]
            [ring.util.response :as ring-resp]
            [meyer.lambda123.config :as config]))

(def pre-body-params
  (interceptor/interceptor
   {:name  ::pre-body-params
    :enter (fn [ctx]
             (if (get-in ctx [:request :content-type])
               ctx
               (assoc-in ctx [:request :content-type] (or (get-in ctx [:request :headers "content-type"])
                                                          (get-in ctx [:request :headers "Content-Type"])))))}))

  (def json-body
    (interceptor/interceptor
     {:name ::json-body
      :leave (fn [ctx]
               (if (contains? ctx :aws.apigw/event)
                 (let [response (:response ctx)
                       body (:body response)
                       content-type (get-in response [:headers "Content-Type"])]
                   (assoc ctx :response
                          (if (and (coll? body) (not content-type))
                            (-> response
                                (ring-resp/content-type "application/json;charset=UTF-8")
                                (assoc :body (json/generate-string body)))
                            response)))
                 ((:leave http/json-body) ctx)))}))
  
  (defn about-page
    [request]
    (ring-resp/response {:msg (format "Clojure %s - served from %s"
                                      (clojure-version)
                                      (route/url-for ::about-page))}))

  (defn home-page
    [request]
    (ring-resp/response {:msg "Hello World!"
                         :params (:params request)}))

  
(def common-interceptors [pre-body-params (body-params/body-params) json-body])


(def routes (route/expand-routes
             #{["/" :get (conj common-interceptors `home-page)]
               ["/about" :get (conj common-interceptors `about-page)]}))



(defn service
  [config-map]
  (merge (config/consume-config) config-map))
