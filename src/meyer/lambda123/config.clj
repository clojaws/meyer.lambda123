(ns meyer.lambda123.config
  (:require [clojure.java.io :as io]))

(defn consume-config
  ([] (consume-config "config.edn"))
  ([config-path] (read-string (slurp (io/resource config-path)))))
