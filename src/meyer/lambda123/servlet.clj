(ns meyer.lambda123.servlet
  (:require [io.pedestal.http :as http]
            [meyer.lambda123.service :as service])
  (:gen-class
   :main true
   :name meyer.lambda123.Servlet))

(defonce servlet (atom nil))

(defn servlet-service
  [_ request response]
  (http/servlet-service @servlet request response))

(defn servlet-init
  [_ config]
  (reset! servlet (http/servlet-init service/service nil)))

(defn servlet-destroy
  [_]
  (http/servlet-destroy @servlet)
  (reset! servlet nil))

(defn -main
  [& args]
  (http/start servlet-init))
