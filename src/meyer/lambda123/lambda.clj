(ns meyer.lambda123.lambda
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.aws.lambda.utils :as lambda.utils]
            [meyer.lambda123.service :as service]
            [io.pedestal.http.impl.servlet-interceptor :as servlet-utils])
  (:import [com.amazonaws.services.lambda.runtime
            Context RequestHandler RequestStreamHandler]
           [java.io ByteArrayOutputStream])
  (:gen-class
   :methods [^:static [handler [Object com.amazonaws.services.lambda.runtime.Context] Object]]))

(def lambda-service (-> (service/service {:env :lambda
                                          ;; ::http/join? false
                                          ;; :io.pedestal.http/type :jetty
                                          :io.pedestal.http/routes service/routes
                                          :io.pedestal.http/container-options {:h2c? true
                                                                               :h2? false
                                                                               ;; :keystore "test/hp/keystore.jks"
                                                                               ;; :key-password "password"
                                                                               ;; :ssl-port 8443
                                                                               :ssl? false
                                                                               :body-processor (fn [body]
                                                                                                 (if (string? body)
                                                                                                   body
                                                                                                   (->> (ByteArrayOutputStream.)
                                                                                                        (servlet-utils/write-body-to-stream body)
                                                                                                        (.toString))))}})
                        http/default-interceptors
                        lambda.utils/direct-apigw-provider))

(def lambda-service-fn
  (:io.pedestal.aws.lambda/apigw-handler lambda-service))

(defn -handler [^Object req ^Context ctx]
  (lambda-service-fn req ctx))
