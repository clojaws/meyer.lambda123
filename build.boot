(def project 'meyer.lambda123)
(def version "0.1.0-SNAPSHOT")

(set-env!
 :source-paths #{"src"})

(task-options!
 pom {:project project
      :version version})

(require '[clojure.string :as string]
         '[boot.core :as boot])

(deftask build
  "build the project"
  []
  (comp (pom) (jar)))

(defn deps-syntax->legacy-syntax
  [list-of-deps]
  (mapv (fn [[k v :as dep]]
          (apply conj [k (:mvn/version v)] (apply concat (rest v)))) list-of-deps))

(defn deps-aliases->env
  [{:keys [aliases] :as deps-edn} & keynames]
  (let [profiles (select-keys aliases (vec keynames))]
    (reduce (fn [acc [k {:keys [extra-paths main-opts extra-deps jvm-opts]} :as p]]
              (-> (update acc :dependencies #(apply conj % (deps-syntax->legacy-syntax extra-deps)))
                  (update :source-paths #(apply conj % extra-paths))
                  (update :jvm-opts #(apply conj % jvm-opts)))) {:dependencies #{}
                                                                 :source-paths #{}
                                                                 :jvm-opts     #{}} profiles)))

(defn update-sources-path
  [{:keys [source-paths] :as deps-edn}]
  (let [rsc-folder (filter #(or (string/includes? % "config")
                                (string/includes? % "resources")) source-paths)
        src-folder  (remove #(get (set rsc-folder) %) source-paths)]
    (-> (assoc deps-edn :source-paths (set src-folder))
        (assoc :resource-paths (set rsc-folder)))))

(defn deps-edn->env
  ([deps-edn] (deps-edn->env deps-edn :dev))
  ([{:keys [paths deps] :as deps-edn} & profiles]
   (let [dependencies (deps-syntax->legacy-syntax deps)
         from-aliases (apply deps-aliases->env deps-edn profiles)]
     (-> (merge-with into
                     {:dependencies dependencies
                      :source-paths paths}
                     from-aliases)
         update-sources-path))))

(defn local-deps-edn
  ([] (local-deps-edn "deps.edn" :dev))
  ([file-path] (local-deps-edn "deps.edn" :dev))
  ([file-path & profiles]
   (let [edn-deps (read-string (slurp file-path))]
     (apply deps-edn->env edn-deps profiles))))

(deftask build-env
  [p profiles PROFILES #{kw} "Profiles to select"
   f file PATH str "deps.edn path to read"]
  (let [{:keys [source-paths dependencies resource-paths]} (apply local-deps-edn (or "deps.edn" file) profiles)]
    (set-env!
     :dependencies   dependencies
     :source-paths   source-paths
     :resource-paths resource-paths)
    identity))

(deftask run
  []
  (let [_ (require 'meyer.lambda123.server)
        -main (resolve 'meyer.lambda123.server/-main)]
    (-main)
    identity))

(deftask dev
  []
  (comp (build-env :profiles #{:dev :local :lambda}) (repl :init-ns 'meyer.lambda123.server)))

(deftask build-server
  [t type VAL kw "Type of build"]
  (let [profiles (case type
                   :servlet #{}
                   :lambda #{:lambda}
                   #{:local})
        aot-ns (case type
                 :servlet #{'meyer.lambda123.servlet}
                 :lambda #{'meyer.lambda123.lambda}
                 #{'meyer.lambda123.server})
        main-class (case type
                     :servlet 'meyer.lambda123.Servlet
                     :lambda nil
                     'meyer.lambda123.Server)]
    
    (comp (build-env :profiles profiles)
          (aot :namespace aot-ns)
          (uber)
          (jar :main main-class
               :file "lambda123.jar")
          (target))))

(deftask run-jar
  []
  (require 'clojure.java.shell)
  (let [sh (resolve 'clojure.java.shell/sh)
        res (sh "java" "-jar" "target/lambda123.jar")]
    (println res)
    identity))

(deftask deploy-lambda
  []
  (require 'clojure.java.shell)
  (let [sh (resolve 'clojure.java.shell/sh)
        res (sh "awslocal" "lambda" "create-function"
                "--function-name" "lambda123"
                "--runtime" "java8"
                "--handler" "meyer.lambda123.lambda"
                "--zip-file" "fileb://target/lambda123.jar"
                "--role" "arn:aws:iam::123456:role/your-role"
                "--endpoint-url=http://localhost:4574"
                "--region" "us-east-1")]
    (println res)
    identity))

(deftask delete-lambda
  []
  (require 'clojure.java.shell)
  (let [sh (resolve 'clojure.java.shell/sh)
        res (sh "awslocal" "lambda" "delete-function"
                "--function-name" "lambda123"
                "--endpoint-url=http://localhost:4574"
                "--region" "us-east-1")]
    (println res)
    identity))

(deftask invoke-lambda
  []
  (require 'clojure.java.shell)
  (let [sh (resolve 'clojure.java.shell/sh)
        res (sh "awslocal" "lambda" "invoke"
                "--function-name" "lambda123"
                "--invocation-type" "RequestResponse"
                "--log-type" "Tail"
                "--payload" "{}"
                "--endpoint-url=http://localhost:4574"
                "--region" "us-east-1" "out.file")]
    (println res)
    identity))

;; (deftask build-lambda
;;   []
;;   (comp (build-env :profiles #{:lambda})
;;         (aot :namespace #{'meyer.lambda123.Lambda})
;;         (uber)
;;         (jar :main 'meyer.lambda123.Lambda :file "meyer.lambda123.jar")
;;         (target)))

;; (deftask build-servlet
;;   []
;;   (comp (build-env)
;;         (aot :namespace #{'meyer.lambda123.Servlet})
;;         (uber)
;;         (jar :main 'meyer.lambda123.Servlet :file "meyer.lambda123.jar")
;;         (target)))

;; (deftask foo
;;   []
;;   (require 'meyer.lambda123.config)
;;   (let [consume-config (resolve 'meyer.lambda123.config/consume-config)]
;;     (println (consume-config))
;;     identity))
